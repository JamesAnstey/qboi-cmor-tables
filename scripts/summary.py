#!/usr/bin/env python
'''
Summarize requested variables, giving an overview of the data request.

Usage: at linux shell prompt,
    python summary.py
or in ipython session,
    run -i summary.py
'''
###############################################################################
import os
import numpy as np
import json
import copy
###############################################################################

# Names of CMOR tables to load
load_tables = ['mon', 'monZ', 'day', 'dayZ', '6hrPt', '3hr']
# load_tables = ['mon', 'monZ']

activity_id = 'QBOi'

show_parameters = False

show_summary_table = False

if show_summary_table:
    axes = {
        'row' : ['variable_id'],
        'column' : ['table_id'],
        # 'row' : ['dimensions']

    #    'row' : ['table_id', 'variable_id'],
    #    'row' : ['table_id'],
    #    'column' : ['frequency'],
    }

    # The variable 'tables' refers to summary tables, i.e. how to display the info. (Not to CMOR tables!)
    tables = []
    # tables.append('no. of variables')
    # tables.append('unique variable names')
    # tables.append('units')
    # tables.append('variable_id')
    # tables.append('dimensions')
    # tables.append('cell_methods')
    # tables.append('units')
    # tables.append('size')
    # tables.append(('dimensions', 'cell_methods'))
    # tables.append(('dimensions', 'cell_methods', 'units'))
    # tables.append(('long_name', 'units'))
    # tables.append(('long_name', 'comment'))
    tables.append(('long_name', 'dimensions'))

    show_totals = True
    show_number_of_entries = True


# Number of model gridpoints
output = {'latitude' : 91, 'longitude' : 180, 'time' : 42*12}

# Path to CMOR tables json files
path = '../Tables'

# Option to display particular groups of variables (subsets of the total request).
# Some comments on the variable sets & updates to them are in the code comments below where they're defined.
filter_variables = True
variable_sets = []
# variable_sets.append( 'phase-1 GMD, Table 1' )
# variable_sets.append( 'phase-1 GMD, Table 2a' )
# variable_sets.append( 'phase-1 GMD, Table 2b' )
# variable_sets.append( 'phase-1 GMD, Table 2c' )
# variable_sets.append( 'phase-1 GMD, Table 3' )
# variable_sets.append( 'phase-1 GMD, Table 4' )
# variable_sets.append( 'MJO' )
# variable_sets.append('tendencies')

# variable_sets.append( 'phase-2 GMD, climate & variability' )
# variable_sets.append( 'phase-2 GMD, TEM' )
# variable_sets.append( 'phase-2 GMD, GW' )
# variable_sets.append( 'phase-2 GMD, constituents & thermodynamics' )
# variable_sets.append( 'phase-2 GMD, equatorial waves' )
# variable_sets.append( 'phase-2 GMD, MJO' )

variable_sets.append( 'added in phase-2' )

# remove_variable_sets = []
# remove_variable_sets.append( 'phase-1 GMD, Table 1' )
# # remove_variable_sets.append( 'phase-1 GMD, Table 2a' )
# # remove_variable_sets.append( 'phase-1 GMD, Table 2b' )
# # remove_variable_sets.append( 'phase-1 GMD, Table 2c' )
# # remove_variable_sets.append( 'phase-1 GMD, Table 3' )
# # remove_variable_sets.append( 'phase-1 GMD, Table 4' )

# Set max column width, for plaintext table display. Useful to adjust if text wrapping is distorting a large table.
table_column_max_width = 12


make_latex_table = True
if make_latex_table:
    latex_columns = ['variable_id', 'long_name', 'units', 'table_id', 'dimensions']
    # latex_columns = ['variable_id', 'long_name', 'units', 'dimensions']
    combine_on = 'variable_id'
    change_dimensions_to_spatial_dimensions = True

###############################################################################
# functions

def file_size_str(a):
    '''Given file size in bytes, return string giving the size in nice
    human-readable units (like ls -h does at the shell prompt.'''
    m = 1024.
    d_b = {
        'B' : 1.
    ,   'KB': 1 / m
    ,   'MB': 1 / m**2
    ,   'GB': 1 / m**3
    ,   'TB': 1 / m**4
    ,   'PB': 1 / m**5
    }
    # list of units, in order of descending size of the unit
    uo = sorted([(d_b[s], s) for s in d_b])
    # choose the most sensible size to display
    for tu in uo:
        if (a*tu[0]) > 1: break
    su = tu[1]
    a *= tu[0]
    sa = str('%.3g' % a)
    return sa + ' ' + su

def estimate_size(dimensions, output):
    '''
    Return size in bytes of an output file, based on dimensions and number
    of gridpoints per dimension.
    '''
    dims = [s.strip() for s in dimensions.split()]
    bytes_per_float = 4
    n_float = 1.
    for s in dims:
        if s in output:
            n_float *= output[s]
        else:
            n_float *= gridpoints[s]
    return n_float*bytes_per_float

def column_fmt(row, gap=' '*4, underline=[], emptyline=[], indent='', max_len=100, underline_case='', hl=[]):
    '''
    Display information in a nice plaintext table.
    '''
    w = ''
    if not isinstance(row, list):
        print('Input variable "row" must be list.')
        return w
    for col in row:
        for k in range(len(col)):
            s = str( col[k] )
            l = [s]
            if len(s) > max_len:
                l = []
                while len(s) > max_len:
                    s0 = s[:max_len]
                    sep = ' '
                    m = s0[::-1].find(sep)
                    if m == -1:
                        l += [ s0 ]
                        s = s[max_len:]
                    else:
                        m = len(s0)-m-1
                        l += [ s0[:m] ]
                        s = s[m+len(sep):]
                if len(s) > 0:
                    l += [s]
            col[k] = '\n'.join(l)
    ncol = len(row[0])
    for col in row:
        if len(col) != ncol: raise
    col_len = np.zeros(ncol, 'int')
    for col in row:
        for k in range(ncol):
            s = col[k]
            sl = s.split('\n')
            for s in sl:
                col_len[k] = max(col_len[k], len(s))
    l = []
    if underline_case in ['whole column']:
        colwc = []
        for m in col_len:
            colwc += ['-'*m]
    for k in range(len(row)):
        col = row[k]
        l += [ col ]
        if k in underline:
            col0 = []
            for s in col:
                col0 += ['-'*len(s)]
            l += [col0]
            if underline_case in ['repeat first']:
                # take the first underline and repeat it after every subsequent row
                colru = col0
                kru = k
        if         underline_case in ['repeat first']:
            if k > kru:
                l += [colru]
        elif    underline_case in ['whole column']:
            l += [colwc]
        if k in emptyline:
            col0 = []
            for s in col:
                col0 += ['']
            l += [col0]        
    row = l    
    use_lr = True
    if use_lr:
        lr = []
        wr = ''
    for k in range(len(row)):
        col0 = row[k]
        col1 = []
        more_cols = True
        while more_cols:
            w += indent
            if use_lr: wr += indent
            more_cols = False
            for n in range(ncol):
                s = col0[n]
                m = s.find('\n')
                if m != -1:
                    col1 += [ s[m+1:] ]
                    more_cols = True
                    s = s[:m]
                else:
                    col1 += [ '' ]
                fmt0 = '{:<'+str(col_len[n])+'}'
                s = fmt0.format(s)
                w += s + gap
                if use_lr: wr += s + gap
            w += '\n'
            if use_lr:
                lr += [wr]
                if k in hl:
                    # Put a horizontal line underneath. 
                    # If a row spans more than one row of text, this doesn't place the line correctly (3apr.17).
                    lr += ['-'*len(wr)]
                wr = ''
                
            col0 = col1
            col1 = []
    if use_lr:
        w = ''
        for wr in lr: 
            w += wr + '\n'
    return w

def varkey(variable_id, table_id):
    ''' Return string (variable_id, table_id) that uniquely identifies a variable.'''
    return '{}.{}'.format(variable_id, table_id)

def chksme(x):
    '''Return unique entries of list x (filter out duplicate entries).'''
    return [x[m] for m in filter(lambda m: x[m] not in x[:m], range(len(x)))]

###############################################################################

axes_sort = {}
if filter_variables:
    # Display particular groups of variables.
    # Used this to check consistency with tables in the QBOi GMD paper. 
    keep = set()
    keep_by_set = {variable_set : set() for variable_set in variable_sets}
    # phase-1 GMD paper = Butchart et al. 2018, https://doi.org/10.5194/gmd-11-1009-2018
    convert_variable_id = {
        # For the variable sets from the phase-1 GMD paper, the variable names used in that paper are specified below.
        # In some cases these have been updated to new names for consistency with CMIP6, which includes the stratospheric
        # variables as requested by DynVarMIP (Gerber & Manzini 2016, https://doi.org/10.5194/gmd-9-3413-2016).
        # This dict maps from old names to updated names (old name : new name).
        # In the summary tables displayed by this script, the updated names are used.
        'vstar' : 'vtem',
        'wstar' : 'wtem',
        'fy' : 'epfy', # phase-1 GMD requested fy in kg/s2 (aka N/m), CMIP6 requests epfy in m3/s2
        'fz' : 'epfz', # phase-1 GMD requested fz in kg/s2 (aka N/m), CMIP6 requests epfz in m3/s2
        'utenddivf' : 'utendepfd',
        'psistar' : 'psitem',
        'zmtnt' : 'tntmp', # phase-1 GMD requested zmtnt, but in DynVarMIP tntmp is the diabatic heating rate from model physics
        'tntlw' : 'tntrl',
        'tntsw' : 'tntrs',
        'wa' : 'wap',  # phase-1 GMD requested wa in m/s (GMD Table 4), CMIP6 requests wap (dp/dt, in Pa/s).
        'OLR' : 'rlut',
    }
    for variable_set in variable_sets:
        include = []
        if variable_set == 'phase-1 GMD, Table 1':
            include.append({
                'variable_id' : ['psl', 'prc', 'pr', 'tas', 'uas', 'vas', 'ta', 'ua', 'zg'],
                'table_id' : ['day', 'mon'],
            })
        elif variable_set == 'phase-2 GMD, climate & variability':
            # as 'phase-1 GMD, Table 1', but adding:
            # va.day
            # va.mon            
            include.append({
                'variable_id' : ['psl', 'prc', 'pr', 'tas', 'uas', 'vas', 'ta', 'ua', 'va', 'zg'],
                'table_id' : ['day', 'mon'],
            })

        elif variable_set in 'phase-1 GMD, Table 2a':
            # For EP fluxes, phase-1 GMD requested fy,fz in kg/s2, consistent with AHL87 Eq. 3.5.3a,b.
            # (AHL87 = Middle Atmosphere Dynamics textbook by Andrews, Holton & Leovy, 1987)
            # DynVarMIP (CMIP6) requests epfy,epfz in m3/s2, avoiding the density factor in front (kg/s2 = kg/m3 * m3/s2).
            include.append({
                'variable_id' : ['ua', 'ta', 'zg', 'vstar', 'wstar', 'fy', 'fz', 'utenddivf', 'utend', 'utendogw', 'utendnogw', 'psistar'],
                'table_id' : ['dayZ', 'monZ'],
            })
        elif variable_set == 'phase-2 GMD, TEM':
            # as 'phase-1 GMD, Table 2a', but adding:
            # utendmp.dayZ
            # utendmp.monZ
            # utendnd.dayZ
            # utendnd.monZ
            # utendvtem.dayZ
            # utendvtem.monZ
            # utendwtem.dayZ
            # utendwtem.monZ            
            include.append({
                'variable_id' : ['ua', 'ta', 'zg', 'vstar', 'wstar', 'fy', 'fz', 'utenddivf', 'utend', 'utendogw', 'utendnogw', 'psistar', 'utendmp', 'utendnd', 'utendvtem', 'utendwtem'],
                'table_id' : ['dayZ', 'monZ'],
            })

        elif variable_set == 'phase-1 GMD, Table 2b':
            include.append({
                'variable_id' : ['utendogw', 'utendnogw', 'vtendogw', 'vtendnogw', 'taunoge', 'taunogs', 'taunogw', 'taunogn'],
                'table_id' : ['mon'],
            })
        elif variable_set == 'phase-1 GMD, Table 2c':
            # Note there's a typo in the phase-1 GMD paper Table 2c saying that taunog[e,s,w,n] are 3D. These are launch fluxes, so they
            # should be 2D (XYT fields), as given in the QBOi phase-1 protocol pdf (QBOI_protocol_v1.25_21Nov2016_final.pdf).
            # (However the same variable names appear in Table 2b, as 3D monthly means.)
            include.append({
                'variable_id' : ['tauogu', 'tauogv', 'taunoge', 'taunogs', 'taunogw', 'taunogn'],
                'table_id' : ['day'],
            })
        elif variable_set == 'phase-2 GMD, GW':
            # combining the two gravity wave tables from phase-1 GMD, table 2b & 2c
            include.append({
                'variable_id' : ['utendogw', 'utendnogw', 'vtendogw', 'vtendnogw', 'tauogu', 'tauogv', 'taunoge', 'taunogs', 'taunogw', 'taunogn'],
                'table_id' : ['mon', 'day'],
            })

        elif variable_set in 'phase-1 GMD, Table 3':
            include.append({
                'variable_id' : ['hus', 'zmtnt', 'tntlw', 'tntsw', 'o3'],
                'table_id' : ['dayZ', 'monZ'],
            })
        elif variable_set == 'phase-2 GMD, constituents & thermodynamics':
            # as 'phase-1 GMD, Table 3', but adding:
            # hus.mon
            include.append({
                'variable_id' : ['hus', 'zmtnt', 'tntlw', 'tntsw', 'o3'],
                'table_id' : ['dayZ', 'monZ', 'mon'],
            })

        elif variable_set in ['phase-1 GMD, Table 4',
                                'phase-2 GMD, equatorial waves']:
            # phase-1 GMD requested 'wa', but DynVarMIP requests 'wap' (Table A2 of https://doi.org/10.5194/gmd-9-3413-2016).
            # 'wap' (dp/dt) seems better since wa = dz/dt = -(H/p)*dp/dt involves conversion factor H; data providers would need to document the value used.
            # Also better for consistency with CMIP6 data request. 
            include.append({
                'variable_id' : ['ta', 'ua', 'va', 'wa'],
                'table_id' : ['6hrPt'],
            })
        elif variable_set in ['MJO', 
                                'phase-2 GMD, MJO']:
            include.append({
                'variable_id' : ['pr', 'OLR'],
                'table_id' : ['3hr'],
            })
            include.append({
                'variable_id' : ['ua200', 'va200', 'va850', 'OLR'],
                'table_id' : ['day'],
            })

        elif variable_set == 'tendencies':
            include.append({
                'variable_id' : ['utendepfd', 'utendmp', 'utendnd', 'utendnogw', 'utendogw', 'utendvtem', 'utendwtem', 'vtendnogw', 'vtendogw'],
                'table_id' : list(load_tables),
            })

        elif variable_set == 'added in phase-2':

            include.append({
                'variable_id' : ['va'],
                'table_id' : ['day', 'mon'],
            })

            include.append({
                'variable_id' : ['utendmp', 'utendnd', 'utendvtem', 'utendwtem'],
                'table_id' : ['dayZ', 'monZ'],
            })

            include.append({
                'variable_id' : ['hus'],
                'table_id' : ['mon'],
            })

            # MJO (Kyoto 2017 workshop)
            include.append({
                'variable_id' : ['pr', 'OLR'],
                'table_id' : ['3hr'],
            })
            include.append({
                'variable_id' : ['ua200', 'va200', 'va850', 'OLR'],
                'table_id' : ['day'],
            })

        else:
            raise Exception('Unknown variable set: '+ variable_set)

        for d in include:
            use_variable_id, use_table_id = d['variable_id'], d['table_id']
            for k,variable_id in enumerate(use_variable_id):
                if variable_id in convert_variable_id:
                    use_variable_id[k] = convert_variable_id[variable_id]
            for variable_id in use_variable_id:
                for table_id in use_table_id:
                    assert table_id in load_tables
                    #keep.add(varkey(variable_id, table_id))
                    keep_by_set[variable_set].add( varkey(variable_id, table_id) )
        keep.update( keep_by_set[variable_set] )

        if len(variable_sets) == 1 and axes['row'] == ['variable_id']:
            # Sort variable rows to allow easy comparison with Tables 1-4 in phase-1 GMD paper
            axes_sort['row'] = []
            for d in include:
                axes_sort['row'] += list(d['variable_id'])

        del include

# Load CMOR table json files.
variables = {} # dict keyed by unique variable id, containing parameter values for the variable (units, dimensions, etc)
params = set() # set of parameter names (such as units, dimensions, etc)
tables_loaded, variables_loaded, variables_kept = set(), set(), set()
for table_id in load_tables:
    filename = '{}_{}.json'.format(activity_id, table_id)
    filepath = os.path.join(path, filename)
    if not os.path.exists(filepath):
        print('File not found: ' + filepath)
        continue
    d = {}
    with open(filepath, 'r') as f:
        d = json.load(f)['variable_entry']
        print('Opened file {} containing {} variables'.format(filepath, len(d)))
        tables_loaded.add(table_id)
    for variable_id in d:
        key = varkey(variable_id, table_id)
        variables_loaded.add(key)
        if filter_variables:
            if key not in keep:
                continue
        assert key not in variables, 'Variable is already defined: ' + key
        variables[key] = d[variable_id]
        variables[key].update({'table_id' : table_id, 'variable_id' : variable_id})
        params.update(set(d[variable_id].keys()))
        variables_kept.add(key)
    del d
print('\nLoaded {} tables containing {} variables in total'.format(len(tables_loaded), len(variables_loaded)))
if filter_variables:
    # print('Retained variable set: {s} ({n} variables)'.format(n=len(variables_kept), s=variable_set))
    print('Retained variable sets:')
    n = 0
    for variable_set in variable_sets:
        kept = variables_kept.intersection( keep_by_set[variable_set] )
        print('  {s} ({n} variables)'.format(n=len(kept), s=variable_set))
        n += len(kept)
    if len(variable_sets) > 1:
        print('  ({n} variables in total)'.format(n=n))
if show_parameters:
    print('\nParameter names (these can be used as summary table rows/columns):')
    for p in sorted(params, key=str.lower):
        print('  ' + p)

# Get info on coordinate grids (e.g. number of requested pressure levels).
filename = '{}_coordinate.json'.format(activity_id)
filepath = os.path.join(path, filename)
with open(filepath, 'r') as f:
    coords = json.load(f)['axis_entry']
vertical_grids = [s for s in coords if coords[s]['axis'] == 'Z']
gridpoints = {}
for s in vertical_grids:
    a = coords[s]['requested']
    if isinstance(a, list):
        gridpoints[s] = len(a)
    else:
        gridpoints[s] = 1
gridpoints.update({
    'qboiplevm' : 60
})


###############################################################################
if show_summary_table:

    # Initialize summary table for the specified axes.
    d_axes = {}
    for ax in axes: # ax = 'row' or 'column'
        lp = axes[ax] # one or more parameter names to use for the axis (e.g. 'variable_id', or a combo like ['dimensions', 'table_id'])
        if not isinstance(lp, (list, tuple)): lp = list(lp)
        d_axes[ax] = {}
        for key in variables:
            v = '.'.join([variables[key][p] for p in lp]) # parameter values for this variable, for this axis
            if v not in d_axes[ax]:
                d_axes[ax][v] = set()
            d_axes[ax][v].add(key) # add variable to the row or column corresponding to its parameter values
    axes_values = {ax: sorted(d_axes[ax].keys(), key = lambda v : v.lower()) for ax in axes}
    if len(axes_sort) > 0:
        for ax in axes_sort:
            # reorder the rows, probably to match the tables in Butchart et al. 2018
            axes_sort[ax] = chksme(axes_sort[ax])
            axes_values[ax] = [s for s in axes_sort[ax] if s in axes_values[ax]]
            assert len(axes_values['row']) > 0

    summary_table = {}
    for vr in axes_values['row']:
        for vc in axes_values['column']:
            # each summary table entry (meeting of a row & column) is the intersection of the row,column variable sets
            summary_table[(vr,vc)] = d_axes['row'][vr].intersection(d_axes['column'][vc])
    # At this point the variables have been organized into their positions on the summary table.

    # Get info to be displayed in the summary table (e.g. size of variables).
    indent = ' '*2
    d_table = {}
    for table_type in tables:
        tab = copy.deepcopy(summary_table)
        rows = list(axes_values['row'])
        columns = list(axes_values['column'])
        shp = (len(rows), len(columns))
        
        atab = np.zeros(shp)
        atab.fill(np.nan)
        
        if isinstance(table_type, str):
            table_types = [table_type]
        elif isinstance(table_type, (tuple,list)):
            table_types = list(table_type)
        else:
            raise Exception('table_type: type error')
        del table_type
        entry_sep = ' / '

        formatter = None
        skip_table = False
        for kr,vr in enumerate(rows):
            for kc,vc in enumerate(columns):
                entries = []                
                for table_type in table_types:
                    a = ''
                    if table_type == 'no. of variables':
                        a = len(tab[(vr,vc)])
                        atab[kr,kc] = a
                        formatter = '{:.0f}'.format
                        a = formatter(a)
                    elif table_type == 'size':
                        total_size = 0
                        for key in tab[(vr,vc)]:
                            total_size += estimate_size(variables[key]['dimensions'], output)
                        atab[kr,kc] = total_size
                        formatter = file_size_str
                        if total_size == 0:
                            a = '-'
                        else:
                            a = formatter(total_size)
                    elif table_type == 'unique variable names':
                        lv = tab[(vr,vc)]
                        a = ', '.join(sorted(lv, key = lambda v : v.lower()))
                    elif table_type in ['unique values of ' + p for p in params] + list(params):
                        lv = []
                        p = table_type.split()[-1]
                        for key in tab[(vr,vc)]:
                            v = variables[key][p]
                            if v not in lv: 
                                lv += [str(v)]
                        lv = sorted(lv, key = lambda v : v.lower())
                        if lv == []:
                            a = '-'
                            atab[kr,kc] = 0 # setting this so that show_number_of_entries option, below, works properly
                        else:
                            a = ', '.join(lv)
                    else:
                        skip_table = True
                    if skip_table: break
                    entries.append(a)
                
                if skip_table: break
                tab[(vr,vc)] = entry_sep.join(entries)
            if skip_table: break 
        if skip_table:
            print('Unknown table type: ' + str(table_types))
            continue

        ncol = len(columns)
        nrow = len(rows)

        if np.any(np.isnan(atab)):
            show_totals = False
        if show_totals:
            col_sum = atab.sum(1) # for each row, this is the sum over all columns
            row_sum = atab.sum(0) # for each column, this is the sum over all rows
            all_sum = atab.sum()
            if formatter is not None:
                col_sum = np.array([ formatter(m) for m in col_sum ])
                row_sum = np.array([ formatter(m) for m in row_sum ])
                all_sum = formatter(all_sum)
            s = 'TOTAL:'
            lt = [(s, c) for c in columns]
            rows += [s]
            for k,t in enumerate(lt):
                tab[t] = row_sum[k]
            col_sum = np.concatenate((col_sum, np.array([all_sum])))
            s = 'TOTAL'
            lt = [(r, s) for r in rows]
            columns += [s]
            for k,t in enumerate(lt):
                tab[t] = col_sum[k]

        if show_number_of_entries:
            b = np.where( atab != 0 )
            a = np.zeros(atab.shape, int)
            a[b] = 1
            col_sum = a.sum(1) # for each row, this is the sum over all columns
            row_sum = a.sum(0) # for each column, this is the sum over all rows
            all_sum = a.sum()
            s = 'entries:'
            lt = [(s, c) for c in columns[:ncol]]
            rows += [s]
            for k,t in enumerate(lt):
                tab[t] = row_sum[k]
            col_sum = np.concatenate((col_sum, np.array([all_sum])))
            s = 'entries'
            lt = [(r, s) for r in rows[:nrow]]
            columns += [s]
            for k,t in enumerate(lt):
                tab[t] = col_sum[k]

        # Display the summary table   
        lw = []
        s = entry_sep.join([s for s in table_types])
        lw.append( '\n' + indent + s + ':' )
        lw.append(        indent + '-'*(len(s) + 1) )
        show_row_count = True
        row = []
        col0 = ['']
        col0 += list(columns)
        
        if show_row_count: col0 = [''] + col0
        row += [col0]

        for k,vr in enumerate(rows):
            col = [vr]
            for vc in columns:
                t = (vr,vc)
                if t in tab:
                    col.append(tab[t])
                else:
                    col.append('')
            if show_row_count: 
                no_show = ['total', 'entries']
                if any([col[0].lower().startswith(s) for s in no_show]): s = ''
                else: s = str(k+1) + '.'
                col = [s] + col
            row += [col]
        underline = [0]
        # emptyline = []
        emptyline = range(len(row)) # this will put an empty line between every table line (can be easier to read sometimes)
        if show_totals: 
            if show_number_of_entries:
                emptyline += [len(row)-3]
            else:
                emptyline += [len(row)-2]
        lw.append( column_fmt(row, underline=underline, emptyline=emptyline, indent=indent, gap=' '*2, max_len=table_column_max_width) )
        w = '\n'.join(lw)

        print(w)

###############################################################################
if make_latex_table:
    # print a table with latex formatting to stdout

    # get the requested column info (latex_columns) for the table
    table_vars = {}
    for var,d in variables.items():
        table_vars[var] = {k:v for k,v in d.items() if k in latex_columns}
        del d

    k_dimensions = 'dimensions'
    if change_dimensions_to_spatial_dimensions and 'dimensions' in latex_columns:
        # make a new variable attribute showing abbreviated spatial dimensions
        k = 'spatial_dimensions'
        for var, d in table_vars.items():
            l = d[k_dimensions].split()
            l = [s for s in l if not s.startswith('time')]
            d[k] = ', '.join(l)
        m = latex_columns.index(k_dimensions)
        latex_columns[m] = k
        k_dimensions = k
        del m,k,l,d

    table_var_groups = {}
    for var,d in table_vars.items():
        key = d[combine_on]
        if key not in table_var_groups: 
            table_var_groups[key] = {k:[] for k in d}
        for k in latex_columns: 
            table_var_groups[key][k].append(d[k])
        del d

    combine_tables_on_one_row_if_dimensions_are_same = True
    if combine_tables_on_one_row_if_dimensions_are_same:
        for var,d in table_var_groups.items():
            if k_dimensions not in d:
                continue
            if len(set(d[k_dimensions])) == 1:
                l = list(d['table_id'])
                for k,s in enumerate(l):
                    d['table_id'][k] = ', '.join(l)

    column_name = {
        'variable_id' : 'Name',
        'long_name' : 'Long name',
        'units' : 'Units',
        'table_id' : 'Table',
        'dimensions' : 'Dimensions',
        'spatial_dimensions' : 'Spatial dimensions',
    }
    column_name.update({s : s for s in latex_columns if s not in column_name})
    headers = [column_name[s] for s in latex_columns]
    rows = [headers]
    # for key,d in table_var_groups.items():
    for key,d in sorted(table_var_groups.items()):
        # each key is one or more rows
        n = len(d[combine_on]) # number of rows from this key
        for m in range(n):
            row = []
            for s in latex_columns:
                if len(set(d[s])) == 1:
                    if m == 0:
                        row.append(d[s][0])
                    else:
                        row.append('')
                else:
                    row.append(d[s][m])
            if set(row) == {''}: # don't add an empty row
                continue
            rows.append(row)
            del row
        del d

    # display plaintext version to check that info is organized how we want
    print(column_fmt(rows, underline=[0]))

    # add latex formatting
    def latex_table(rows, just_tabular=False):
        n = len(rows[0])
        assert all([len(row) == n for row in rows])
        justify = 'l'*n
        sep = ' & '
        rowend = ' \\\\'
        l = []
        if not just_tabular:
            l.append('\\begin{table}[h]')
            l.append('\\caption{TABLE CAPTION}')
        l.append('\\begin{tabular}{' + justify + '}')
        l.append('\\hline')
        for k,row in enumerate(rows):
            columns = sep.join(row)
            l.append(f'{columns}{rowend}')
            if k == 0:
                # assume k=0 are column headers & put a line below them
                l.append('\\hline')
        l.append('\\hline')
        l.append('\\end{tabular}')
        if not just_tabular:
            # l.append('\\belowtable{TABLE FOOTNOTES}')
            l.append('\\label{tab:LABEL}')
            l.append('\\end{table}')
        return '\n'.join(l)
    
    w = latex_table(rows)
    # w = latex_table(rows, just_tabular=True)

    # print(w)
    filename = 'table.tex'
    with open(filename, 'w') as f:
        f.write(w)
        print('wrote ' + filename )

    # check how many of the total variables loaded are accounted for by the variables in this table
    missing = variables_loaded.difference(set(variables.keys()))
    n = len(missing)
    if n > 0:
        print(f'\nNumber of variables loaded that are not in the latex table: {n}')
        print('Missing variables:')
        sep = ' '
        # sep = '\n'
        print(sep.join(sorted(missing)))
        



