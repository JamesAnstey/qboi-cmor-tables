#!/usr/bin/env python
'''
Compare CMOR tables (data requests) of two or more projects.
Produces json files & a spreadsheet comparing them.

Requires path to dir containing CMOR tables (json files) for each project.
Tables are typically in a Tables dir from a repo, e.g.
    git clone git@github.com:cedadev/ccmi-2022.git
and then set the path to 'ccmi-2022/Tables'.
'''

import os
import json
import copy
import openpyxl as xp

projects = {
    'QBOi' : {
        'path' : 'qboi-cmor-tables/Tables',
        },
    'CCMI2022' : {
        'path' : 'ccmi-2022/Tables',
    }
}

summary = {
    'frequencies' : set(),
    'variables' : set(),
}

for project, project_info in projects.items():
    path = project_info['path']
    ld = os.listdir(path)

    # get table names
    # assume filenames are '{project}_{table}.json'
    filenames = [s for s in ld if s.startswith(project) and s.endswith('.json') and s.count('_') == 1]
    tables = {}
    project_info['tables'] = tables
    for filename in filenames:
        table = filename.split('_')[-1].replace('.json', '') # table name without the project name
        tables[table] = {
            'filename' : filename,
        }
    
    # get info about tables
    for table, table_info in tables.items():
        filepath = os.path.join(path, table_info['filename'])
        with open(filepath, 'r') as f:
            dtab = json.load(f)
        # print(project, table, dtab.keys())
        if 'variable_entry' in dtab:
            table_info['variables'] = sorted(dtab['variable_entry'].keys())
            summary['variables'].update(table_info['variables'])
            table_info['no. of variables'] = len(table_info['variables'])
            table_info['frequency'] = set()
            table_info['dimensions'] = {}
            for cmorvar, cmorvar_info in dtab['variable_entry'].items():
                if 'frequency' in cmorvar_info:
                    freq = cmorvar_info['frequency']
                    if freq.endswith('Pt'):
                        freq = freq.replace('Pt', '') # ignore the 'Pt' for purpose of classifying tables by frequency
                    table_info['frequency'].add(freq)
                    del freq
                if 'dimensions' in cmorvar_info:
                    dimensions = cmorvar_info['dimensions']
                    if dimensions not in table_info['dimensions']:
                        table_info['dimensions'][dimensions] = 1
                    else:
                        table_info['dimensions'][dimensions] += 1
            summary['frequencies'].update(table_info['frequency'])
            table_info['frequency'] = sorted(table_info['frequency'])
        del dtab

outfile = 'summary.json'
with open(outfile, 'w') as f:
    json.dump(projects, f, indent=2)
    print('wrote ' + outfile)

# print no. of variables by table
print('\nNumber of variables by project:')
for project, project_info in projects.items():
    print(f'  {project}')
    n_total = 0
    for table, table_info in sorted(project_info['tables'].items()):
        if 'variables' not in table_info:
            continue
        if table == 'grids':
            continue
        n = len(table_info['variables'])
        # print(f'{fmt % n} {table}')
        print(f'    {"%-8s" % table} {"%3i" % n}')
        n_total += n
    print(f'    TOTAL: {"%3i" % n_total}')
print()


# compare_variables = ['ua', 'va', 'ta']
# compare_frequencies = ['mon', 'day']

compare_variables = sorted(summary['variables'], key=str.lower)
compare_frequencies = sorted(summary['frequencies'], key=str.lower)

# get_params = ['frequency', 'units', 'outname', 'dimensions']
get_params = ['frequency', 'units', 'dimensions', 'cell_methods', 'long_name']

comparison = {frequency : {} for frequency in compare_frequencies}
for frequency in compare_frequencies:
    for cmorvar in compare_variables:
        cmorvars = []
        # for project, project_info in projects.items():
        for project, project_info in sorted(projects.items()):
            path = project_info['path']
            # for table, table_info in project_info['tables'].items():
            for table, table_info in sorted(project_info['tables'].items()):
                if 'variables' not in table_info:
                    continue
                if cmorvar in table_info['variables'] and frequency in table_info['frequency']:
                    # get info on the variable from the CMOR table json file
                    filepath = os.path.join(path, table_info['filename'])
                    with open(filepath, 'r') as f:
                        dtab = json.load(f)
                    dvar = dtab['variable_entry'][cmorvar]
                    # if dvar['frequency'] != frequency:
                    #     continue
                    d = {p : '' for p in get_params}
                    d.update({p : dvar[p] for p in get_params if p in dvar})
                    d.update({'project' : project, 'table' : table})
                    cmorvars.append(d)
                    del d
        if len(cmorvars) > 0:
            comparison[frequency][cmorvar] = cmorvars


outfile = 'comparison.json'
with open(outfile, 'w') as f:
    json.dump(comparison, f, indent=2)
    print('wrote ' + outfile)


write_xlsx = True

if write_xlsx:
    outfile = 'comparison.xlsx'

    blank_row_between_vars = False

    # convert info in dict comparison to sheets & rows for display as spreadsheet
    sheets = sorted(comparison.keys(), key=str.lower)
    column_headers = ['variable_id', 'project', 'table'] + get_params
    assert len(set(column_headers)) == len(column_headers) # ensure unique column names
    workbook = {sheet : [] for sheet in sheets}
    for sheet in sheets:
        rows = workbook[sheet]
        rows.append(column_headers) # put column headers in first row
        for cmorvar in comparison[sheet]:
            # for each instance of the variable, add a row to the sheet
            rows_added = 0
            for d in comparison[sheet][cmorvar]:
                row = [cmorvar] + [d[p] for p in column_headers[1:]]
                rows.append(row)
                rows_added += 1
            if blank_row_between_vars and rows_added > 0:
                rows.append(['' for p in column_headers]) # add blank row between variables

    # create spreadsheet
    colors = {
        # https://htmlcolorcodes.com/
        'white' : 'FFFFFF',
        'light grey' : 'D5D5D5',
        'orange' : 'FFCE65',
        'light purple' : 'F9E9FF',
    }
    alternate_color_rows = True

    wb = xp.Workbook()
    wb.remove(wb.worksheets[0])
    for sheet in sheets:
        ws = wb.create_sheet(sheet)
        ws.title = sheet
        
        rows = copy.deepcopy(workbook[sheet])

        set_row_color = {}
        if alternate_color_rows:
            alternate_colors = ['white', 'light purple']
            column_index = 0
            colour_index = 0
            key = rows[1][column_index]
            for k in range(1,len(rows)):
                row = rows[k]
                if row[column_index] != key:
                    colour_index += 1
                    if colour_index == len(alternate_colors):
                        colour_index = 0
                    key = row[column_index]
                set_row_color[k] = alternate_colors[colour_index]

        for k, row in enumerate(rows):
            current_row = k + 1
            ws.append(row)
            if k == 0:
                # format column headers
                for m,s in enumerate(row):
                    current_column = m + 1
                    cell = xp.utils.get_column_letter(current_column) + str(current_row)
                    c = ws[cell]
                    c.font = xp.styles.Font(bold=True) # boldface
                    color = colors['orange']
                    c.fill = xp.styles.PatternFill(start_color=color, end_color=color, fill_type='solid')
            if k in set_row_color:
                color = colors[set_row_color[k]]
                for m,s in enumerate(row):
                    current_column = m + 1
                    cell = xp.utils.get_column_letter(current_column) + str(current_row)
                    c = ws[cell]
                    c.fill = xp.styles.PatternFill(start_color=color, end_color=color, fill_type='solid')

        # Get max column widths and expand columns for legibility if needed
        ncol = len(column_headers)
        assert all([len(row) == ncol for row in rows])
        column_max_widths = {p : 0 for p in column_headers}
        for row in rows:
            column_widths = {p : len(row[k]) for k,p in enumerate(column_headers)}
            for p in column_widths:
                column_max_widths[p] = max(column_max_widths[p], column_widths[p])
        # Set column width for better readability
        for k,p in enumerate(column_headers):
            current_column = k + 1
            s = xp.utils.get_column_letter(current_column)
            ws.column_dimensions[s].width = column_max_widths[p] + 5

    wb.save(filename=outfile)
    # os.chmod(outfile, 0o644)
    print('wrote ' + outfile)

