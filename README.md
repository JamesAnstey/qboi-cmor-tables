# CMOR tables for QBOi data request

These tables incorporate and update the QBOi phase-1 data request from [Butchart et al. 2018](https://gmd.copernicus.org/articles/11/1009/2018).
Future changes in the QBOi data request should be captured in this repository (i.e., under version control). 
Errors, omissions, or any other issues about the data request should be raised in the [issue tracker](https://gitlab.com/JamesAnstey/qboi-cmor-tables/-/issues).

As in CMIP, tables are given as json files in the `Tables` directory, which can be used with the [CMOR software](https://cmor.llnl.gov/). 
QBOi-requested variables are grouped together in tables based on time frequency (monthly, daily, ...) and zonal averaging.
As in CMIP, an output variable is uniquely specified by the combination of its table name and variable name (e.g., `ua_day` for daily zonal wind).

The tables have been created starting from the [CMIP6 tables](https://github.com/PCMDI/cmip6-cmor-tables) and the [SNAPSI tables](https://github.com/aph42/snap).
Variable names and attributes have been made consistent with these where possible.

In the `scripts` directory, `summary.py` can be used to get an overview of the variables.
